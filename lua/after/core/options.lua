local opt = vim.opt 

-- line numbers
opt.relativenumber = true 
opt.number = true 
opt.numberwidth = 2
opt.ruler = false

-- tabs & inditcation
opt.expandtab = true
opt.shiftwidth = 2
opt.smartindent = true
opt.tabstop = 2
opt.softtabstop = 2

-- line wrapping
opt.wrap = false

-- search settings
opt.ignorecase = true
opt.smartcase = true

-- cursor line
opt.cursorline = true

-- appearance
opt.signcolumn = "yes"
opt.termguicolors = true
opt.timeoutlen = 400
opt.undofile = true

-- backspace
opt.backspace = "indent,eol,start"

-- clipboard
opt.clipboard = "unnamedplus"
opt.clipboard:append("unnamedplus")
opt.laststatus = 3 

-- split windows
opt.splitright = true 
opt.splitbelow = true

-- interval for writing swap file to disk, also used by gitsigns
opt.updatetime = 250

-- go to previous/next line with h,l,left arrow and right arrow
-- when cursor reaches end/beginning of line
opt.whichwrap:append "<>[]hl"
