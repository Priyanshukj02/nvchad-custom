vim.g.mapleader = " "
vim.g.maplocalleader = " "
local keymap = vim.keymap

-- insert mode keymap
keymap.set("i", "jk", "<ESC>")
keymap.set("i","<C-b>","<ESC>^i")
keymap.set("i","<C-e>","<End>")
keymap.set("i","<C-h>","<Left>")
keymap.set("i","<C-l>","<Right>")
keymap.set("i","<C-j>","<Down>")
keymap.set("i","<C-k>","<Up>")
keymap.set("i", "kj", "<ESC>")

-- normal mode keymap
keymap.set("n","<ESC>",":noh <CR>")
keymap.set("n", "x", '"_x')
keymap.set("n", "<leader>+", "<C-a>")
keymap.set("n", "<leader>-", "<C-x>")
keymap.set("n", "<leader>sv", "<C-w>v") 
keymap.set("n", "<leader>sh", "<C-w>s") 
keymap.set("n", "<leader>se", "<C-w>=") 
keymap.set("n", "<leader>sx", ":close<CR>")
keymap.set("n", "<leader>to", ":tabnew<CR>")
keymap.set("n", "<leader>tx", ":tabclose<CR>")
keymap.set("n", "<Tab>", ":tabn<CR>") 
keymap.set("n", "<S-Tab>", ":tabp<CR>")
keymap.set("n", "<leader>sm", ":MaximizerToggle<CR>")
keymap.set("n", "<leader>e", "<cmd> NvimTreeFocus <CR>")
keymap.set("n","<C-n>","<cmd> NvimTreeToggle <CR>")
keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<cr>")
keymap.set("n", "<leader>fs", "<cmd>Telescope live_grep<cr>") 
keymap.set("n", "<leader>fc", "<cmd>Telescope grep_string<cr>") 
keymap.set("n", "<leader>fb", "<cmd>Telescope buffers<cr>") 
keymap.set("n", "<leader>fh", "<cmd>Telescope help_tags<cr>")
keymap.set("n", "<leader>gc", "<cmd>Telescope git_commits<cr>") 
keymap.set("n", "<leader>gfc", "<cmd>Telescope git_bcommits<cr>")
keymap.set("n", "<leader>gb", "<cmd>Telescope git_branches<cr>")
keymap.set("n", "<leader>gs", "<cmd>Telescope git_status<cr>")
keymap.set("n", "<leader>rs", ":LspRestart<CR>")
keymap.set("n", "<C-s>", ":w<cr>")
keymap.set("n", "<C-sq>",":wq<cr>")
keymap.set("n", "<C-q>", ":q<cr>")
keymap.set("n","<C-uq>", ":q!<cr>")
keymap.set("n","<leader>fo","<cmd> Telescope oldfiles <CR>")

-- Visual --
-- Stay in indent mode
keymap.set("v", "<", "<gv")
keymap.set("v", ">", ">gv")

-- Move text up and down
keymap.set("v", "<A-j>", ":m .+1<CR>==")
keymap.set("v", "<A-k>", ":m .-2<CR>==")
keymap.set("v", "p", '"_dP')


-- visual Block mode keymaps
keymap.set("x", "J", ":move '>+1<CR>gv-gv")
keymap.set("x", "K", ":move '<-2<CR>gv-gv")
keymap.set("x", "<A-j>", ":move '>+1<CR>gv-gv")
keymap.set("x", "<A-k>", ":move '<-2<CR>gv-gv")

